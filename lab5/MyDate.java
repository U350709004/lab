public class MyDate {

	int day;
	int month;
	int year;
	
	int[] maxDays= {31,29,31,30,31,30,31,31,30,31,30,31};
	
	public MyDate(int day, int month, int year){
		this.day=day;
		this.month=month;
		this.year=year;
	}
	public boolean inLeapYear(){
		return year%4 == 0;
	}
	public void incrementDay(){
		day++;
		if (day>maxDays[month]|| (!inLeapYear() && month==1 && day==29)){
			day=1;
			incrementMonth(); //will adjust the month and year
		}
	}
	public void decrementDay(){
		day--;
		if(day==0){
			day=31;
			decrementMonth(); //will also assign the max day of the reulsti month to the day			
		}
	}
	public void incrementDay(int i){
		while (i>0){
			incrementDay();
			i--;
		}
	
	}
	public void decrementDay(int i){
		while (i>0){
			decrementDay();
			i--;
		}	
	}
	public void incrementMonth(int i){
		int newMonth=(month+i)%12;
		int incrementYear=(month+i) /12;
		int incremenYear=0;
		if(newMonth<0){
			newMonth+=12;
			incrementYear--;
		}
		
		
		
		incrementYear+=(month+i)/12;
		if (day>maxDays[newMonth]){
			day=maxDays[newMonth];
		}else if(month==1 && day ==29 && !inLeapYear()){
			day=28;
		}
		year+=incrementYear;
	}
	public void incrementMonth(){
		incrementMonth(1);
	}
	public void decrementMonth(int i){
		incrementMonth(-i);
	}
	public void decrementMonth(){
		incrementMonth(-1);
	}
	public void incrementYear(int i){
		year+=i;
		if (month==1&& day==29 && !inLeapYear()){
			day=29;
		}
	}
	public void incrementYear(){
		incrementYear(1);
	}
	public void decrementYear(int i){
		incrementYear(-i);
	}
	public void decrementYear(){
		incrementYear(1);
	}
	public boolean isBefore(MyDate anotherDate){
		String thisDate=toString().replaceAll("-", "");
		String antString=anotherDate.toString().replaceAll("-", "");
		if(Integer.parseInt(thisDate)<Integer.parseInt(antString)){
			return true;
		}
		return false;
	}
	public int dayDifference(MyDate anotherDate){
		int diff=0;
		if (isBefore(anotherDate)){
			while(isBefore(anotherDate)){
				diff++;
				anotherDate.decrementDay();
			}
		}else if(isAfter(anotherDate)){
			while (isAfter(anotherDate)){
				diff++;
				anotherDate.incrementDay();
			}
		}
		return diff;
	}
	public boolean isAfter(MyDate anotherDate){
		String thisDate=toString().replaceAll("-", "");
		String antString=anotherDate.toString().replaceAll("-", "");
		if(Integer.parseInt(thisDate)<Integer.parseInt(antString)){
			return true;
		}
		return false;
	}
	
	public String toString(){
		return year+"-"+ ((month+1) <10 ? "0":"")+ (month+1)+
				"-"+(day <10 ? "0":"")+ day;
	}
}